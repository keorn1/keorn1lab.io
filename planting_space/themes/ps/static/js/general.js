
let smallViewport = 1440
const width = $(window).width()
const isMobile = window.orientation > -1



function viewPort() {
    if (width <= 375) {
        calc = (parseFloat(width) / parseFloat(375))
        scale = 0.5 * calc
        scale = scale.toFixed(2)
        $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=' + scale + ', maximum-scale=' + scale + ', width=750')
    } else if (width) {
        $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=.5, maximum-scale=.5, width=750')
    }
}

viewPort()



if (width >= smallViewport) {
    $(".main-container").addClass("fixed")
    window.onload = function () {
        lax.init()

        // Add a driver that we use to control our animations
        lax.addDriver('scrollY', function () {
            return window.scrollY
        })

        // Add animation bindings to elements
        lax.addElements('.logo-big', {
            scrollY: {
                translateY: [
                    [0, 500],
                    [0, -500]
                ],
                scale: [
                    [0, 100],
                    [1, .4]
                ]
            }
        })
    }
} else {
    window.onload = function () {
        lax.init()

        // Add a driver that we use to control our animations
        lax.addDriver('scrollY', function () {
            return window.scrollY
        })

        // Add animation bindings to elements
        lax.addElements('.logo-big', {
            scrollY: {
                translateY: [
                    [0, 440],
                    [0, -415]
                ],
                translateX: [
                    [0, 400],
                    [0, -290]
                ],
                scale: [
                    [0, 400],
                    [1, .15]
                ]
            }
        },
            {
                style: {
                    float: 'left'
                }
            }

        )

        lax.addElements('.logo', {
            scrollY: {
                translateX: [
                    [0, 400],
                    [0, -350]
                ],
                scale: [
                    [0, 400],
                    [1, .13]
                ],
                opacity: [
                    [0, 300],
                    [1, 0]
                ]
            }
        },
            {
                style: {
                    position: 'fixed'
                }
            }

        )
    }
}




function showPersonInfo(e) {
    e.stopPropagation()
    e.preventDefault()
    $(".person-info").hide()
    $(".person-main-name").show()
    $(".trigger-person-info").css('display', 'block')
    var _this = $(this)
    $(".person").addClass("zindex0")
    $(".person").removeClass("zindex")
    $(this).parent().parent().parent().removeClass("zindex0")
    $(this).parent().parent().parent().addClass("zindex")
    $(this).parent().parent().parent().find(".person-main-name").fadeOut(100, function () {
        _this.parent().parent().parent().find(".person-info").fadeIn(500)
    })
}

function hidePersonInfo(e) {
    e.stopPropagation()
    e.preventDefault()
    var _this = $(this)
    $(this).find(".person-info").fadeOut(200, function () {
        _this.find(".person-main-name").fadeIn(200)
    })
}
function onClickTogglePersonInfo(e) {
    e.stopPropagation()
    e.preventDefault()
    var _this = $(this)

    if ($(this).parent().parent().parent().find(".person-info").css('display') === "none") {
        $(".person-info").hide()
        $(".person-main-name").show()
        $(".person").addClass("zindex0")
        $(".person").removeClass("zindex")
        $(this).parent().parent().parent().removeClass("zindex0")
        $(this).parent().parent().parent().addClass("zindex")
        $(this).parent().parent().parent().find(".person-main-name").fadeOut(100, function () {
            _this.parent().parent().parent().find(".person-info").fadeIn(500)
        })
    } else if ($(this).parent().parent().parent().find(".person-info").css('display') === "block") {
        $(this).parent().parent().parent().find(".person-info").fadeOut(200, function () {
            _this.parent().parent().parent().find(".person-main-name").fadeIn(200)
        })
    }
}


$(function () {
    if (isMobile) {
        less.modifyVars({
            '@for-desktop': '~"screen and (min-width: 4000px)"',
        })
        smallViewport = 4000
    }

    if (width >= smallViewport) {
        $(".menu").addClass('collapsed')
        $(document).on("mouseenter", ".person .animation div", showPersonInfo)
    }


    $(document).on("click", ".person .animation div", onClickTogglePersonInfo)
    $(document).on("mouseleave", ".person", hidePersonInfo)

    $(document).on("click", ".menu-button", function () {
        $(".menu").toggleClass("collapsed")
    })


    $(document).on("click", '.contact,a[href="#contact"]', function () {

        $('html, body').animate({
            scrollTop: $(".footer-header").offset().top
        }, 2000)
    })

    //load feed
    if ($(".feeds").length > 0) {
        var isAny = false
        $.ajax({
            url: "https://mas.to/users/PlantingSpace.rss",
            dataType: 'xml'
        }).done(function (xmlDoc) {
            $xml = $(xmlDoc)
            $item = $xml.find("item")
            $.each($xml.find("item"), function (index, value) {
                $date = $(this).find('pubDate').text().split(' ').slice(1, 4).toString().replace(/,/g, ' ')
                $txt = $(this).find('description').text()
                if ($txt != "") {
                    isAny = true
                    if (index == 0) {
                        $feed = $(".feed:eq(0)")
                        $feed.find(".feed-content").html($txt)
                        $feed.find(".feed-date").html($date)
                    } else {
                        $feed = $(".feed:eq(0)").clone()
                        $feed.find(".feed-content").html($txt)
                        $feed.find(".feed-date").html($date)
                        $(".feeds").append($feed)
                    }
                }
            })
            if (!isAny) {
                $(".feed:eq(0)").remove()
            }
        })
    }
})


$(function () {
    if (width >= smallViewport) {
        $(".menu").addClass('collapsed')
    }
    //load feed
})

